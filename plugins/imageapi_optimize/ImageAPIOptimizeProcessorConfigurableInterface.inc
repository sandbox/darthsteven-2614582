<?php

interface ImageAPIOptimizeProcessorConfigurableInterface extends ImageAPIOptimizeProcessorInterface {
  public function configForm();
}